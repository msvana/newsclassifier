"""
Downloads the news dataset from aktuality.sk
"""

import csv
import sys
import time

import bs4
import requests

BASE_URL = 'https://spravy.aktuality.sk/'
USER_AGENT = 'Milos Svana <milos.svana@gmail.com> Downloading for research purposes.'
REQUEST_WAIT_TIME = 4
REQUEST_HEADERS = {'User-Agent': USER_AGENT}
PAGE_LIMIT = 5000


def main():
    output_path = sys.argv[1]
    start_page = int(sys.argv[2]) if len(sys.argv) > 2 else 2
    print('Getting number of pages')
    num_pages = get_num_pages(BASE_URL)
    print('We can read up %d pages. We are limited to %d pages' % (num_pages, PAGE_LIMIT))
    pages_to_read = min(start_page + num_pages, PAGE_LIMIT)

    with open(output_path, 'a+') as output:
        csv_writer = csv.writer(output, delimiter=';', quotechar='"')
        for page_current in range(start_page, pages_to_read + start_page):
            print('Getting news from page %d' % page_current)
            page_url = '%s%d/' % (BASE_URL, page_current)
            article_items = get_news_from_page(page_url)

            for article_item in article_items:
                article_record = parse_article_item(article_item)
                csv_writer.writerow(article_record.values())


def get_num_pages(base_url: str) -> int:
    news_html = do_request(base_url)
    news_soup = bs4.BeautifulSoup(news_html, 'lxml')
    paginator = news_soup.find('div', {'class': 'paging'})
    link_last_page = paginator.find_all('a')[-1]
    num_pages_string = link_last_page.text.strip()
    num_pages = int(num_pages_string)
    return num_pages


def get_news_from_page(url: str) -> list:
    news_html = do_request(url)
    news_soup = bs4.BeautifulSoup(news_html, 'lxml')
    article_items = news_soup.find_all('div', {'class': 'article-item-wrapper'})
    return article_items


def parse_article_item(article_item: bs4.BeautifulSoup) -> dict:
    title_element = article_item.find('a', {'class': 'article-title'})
    title = title_element.text.strip()
    url = title_element['href']
    perex = article_item.find('span', {'class': 'article-perex'}).text.strip()
    time_published = article_item.find('span', {'class': 'article-time'}).text.strip()
    image_url = article_item.find('a', {'class': 'article-image'}).find('img')['data-1024']
    category = article_item.find('a', {'class': 'article-category'}).text.strip()
    article_record = {
        'category': category,
        'title': title,
        'perex': perex,
        'time_published': time_published,
        'url': url,
        'image_url': image_url
    }
    return article_record


def do_request(url: str) -> str:
    time.sleep(REQUEST_WAIT_TIME)
    response = requests.get(url, headers=REQUEST_HEADERS)
    if response.status_code != 200:
        raise ConnectionError('Response status code is %d' % response.status_code)
    return response.content


if __name__ == '__main__':
    main()
