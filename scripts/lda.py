import gensim.corpora
import gensim.models
import nltk.corpus
import nltk.tokenize
import pandas

DATASET_PATH = 'data/news.csv'
NUM_TOPICS = 50


def main():
    stopwords = nltk.corpus.stopwords.words('slovak')
    news = pandas.read_csv(DATASET_PATH, header=None, delimiter=';')
    titles = news[1]
    titles_tokens = titles.apply(lambda x: nltk.tokenize.word_tokenize(x.lower(), language='czech'))
    titles_tokens = titles_tokens.apply(lambda x: list(
        filter(lambda y: y.isalpha() and y not in stopwords, x)))
    titles_tokens = titles_tokens.array
    dictionary = gensim.corpora.Dictionary(titles_tokens)
    dictionary.filter_extremes()
    corpus = [dictionary.doc2bow(title) for title in titles_tokens]

    lda_model = gensim.models.LdaModel(
        corpus=corpus, num_topics=NUM_TOPICS, id2word=dictionary,
        passes=20, alpha='auto', eta='auto')

    for i, topic in lda_model.show_topics(formatted=True, num_topics=NUM_TOPICS, num_words=10):
        print('Topic: %d' % i)
        print(topic)
        print('-----')

    print(lda_model[corpus[0]])
    print(lda_model[corpus[1]])


if __name__ == '__main__':
    main()
